var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');


var _data = [];
var _type = '';
var _resetButton = 'hidden';
var _turnName = '';
var _winnerStatus = '';
var _winnerName = '';

resetFieldSet();

function setType(){
    _type = _type !== 'cross' ? 'cross' : 'round'
    _turnName = _type == 'cross' ? 'Крестики' : 'Нолики';
}

function resetFieldSet() {
    for (i = 1; i < 4; i++) {
        _data[i] = {};
        for (j = 1; j < 4; j++) {
            _data[i][j] = {
                id: i.toString() + j.toString(),
                type: 'empty',
            }
        }
    }

    _type = 'cross';
    _resetButton = 'hidden';
    _turnName = 'Крестики';
    _winnerStatus = '';
    _winnerName = '';
}

function setField(rawIndex, columnIndex) {
    console.log(rawIndex, columnIndex);
    if(_data[rawIndex][columnIndex]['type'] == 'empty' && _winnerStatus == ''){
        _data[rawIndex][columnIndex]['type'] = _type;
        setType();
        _resetButton = 'show';
        checkWinner();
    }
}

function checkWinner() {
    createCheckCollection('round');
    createCheckCollection('cross');
    if(_winnerStatus == 'cross'){
        _winnerName = 'Крестики';
    }

    if(_winnerStatus == 'round'){
        _winnerName = 'Нолики';
    }
}

function createCheckCollection(typeName){
    for(var rawKey in _data){
        var raw = 0;
        for(var columnKey in _data[rawKey]){
            if(_data[rawKey][columnKey]['type'] == typeName){
                raw++
            }
        }
        if(raw == 3) {
            _winnerStatus = typeName;
            break;
        }
    }

    for(i = 1; i < 4; i++){
        var column = 0;
        for(var columnKey in _data){
            if(_data[columnKey][i]['type'] == typeName){
                column++
            }
        }

        if(column == 3) {
            _winnerStatus = typeName;
            break;
        }
    }

    var middle = 0;
    if(middle != 3){
        middle = 0;
        if(_data[1][1]['type'] == typeName){
            middle++;
        }

        if(_data[2][2]['type'] == typeName){
            middle++;
        }

        if(_data[3][3]['type'] == typeName){
            middle++;
        }

        if(middle == 3) {
            _winnerStatus = typeName;
        }
    }

    if(middle != 3){
        middle = 0;
        if(_data[3][1]['type'] == typeName){
            middle++;
        }

        if(_data[2][2]['type'] == typeName){
            middle++;
        }

        if(_data[1][3]['type'] == typeName){
            middle++;
        }

        if(middle == 3) {
            _winnerStatus = typeName;
        }
    }

}

var Store = assign({}, EventEmitter.prototype, {
  /**
   * Get the entire collection of TODOs.
   * @return {object}
   */
    getAll: function() {
        return _data;
    },
    getResetButton: function() {
        return _resetButton;
    },
    getTurnName: function() {
        return _turnName;
    },

    getWinnerName: function() {
        return _winnerName;
    },

    emitChange: function () {
        this.emit('change');
    },

    addChangeListener: function (callback) {
      this.on('change', callback);
    }

});

// Register callback to handle all updates
AppDispatcher.register(function(action) {
    var rawIndex, columnIndex;

    switch(action.actionType) {
        case 'setField':
            rawIndex = action.rawIndex;
            columnIndex = action.columnIndex;
            setField(rawIndex, columnIndex);
            Store.emitChange();
            break;

        case 'reset':
            resetFieldSet();
            Store.emitChange();
            break;
        case 'destroy':
            destroy(action.id);
            Store.emitChange();
        default:
      // no op
  }
});

module.exports = Store;

