var express = require('express');
var router = express.Router();
var form = require('express-form');
var field = form.field;
var joinPath = require('path.join');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/NoughtsAndCrosses', function(req, res) {
  var newPath = joinPath(__dirname, '..', 'views/NoughtsAndCrosses/index.html');
  res.sendFile(newPath);
});



router.post('/mail/send',
  form(
      field("name").trim().required(),
      field("phone").trim().required().is(/^[0-9]+$/),
      field("email").trim().isEmail(),
      field("message").trim().required()
  ),

  // Express request-handler now receives filtered and validated data
  function(req, res){
    if (!req.form.isValid) {
      // Handle errors
      console.log(req.form.errors);

    } else {
      // Or, use filtered form data from the form object:
      console.log("name:", req.form.name);
      console.log("phone:", req.form.phone);
      console.log("email:", req.form.email);
      console.log("message:", req.form.message);
      res.mailer.send('email', {
        to: req.form.email,
        subject: 'Форма обратной связи pechatny.su', // REQUIRED.
        name: req.form.name,
        phone: req.form.phone,
        email: req.form.email,
        message: req.form.message
      }, function (err) {
        if (err) {
          // handle error
          console.log(err);
          res.send('There was an error sending the email');
          return;
        }
        res.send('Email Sent');
      });
    }
  }
);



module.exports = router;
