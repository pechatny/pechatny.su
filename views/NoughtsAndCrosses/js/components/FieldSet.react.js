// var Header = require('./Header.react');
// var List = require('./List.react');
var React = require('react');
var Item = require('./Item.react');

var FieldSet = React.createClass({
    render: function(){
        var raws  = [];
        var data = this.props.data;
        for(var rawKey in data){
            var tds = [];
            for(var columnKey in data[rawKey]){
                tds.push(<Item
                    key={data[rawKey][columnKey]['id']}
                    id={data[rawKey][columnKey]['id']}
                    type={data[rawKey][columnKey]['type']}
                    rawIndex={rawKey}
                    columnIndex={columnKey}
                />);
            }
            raws.push(<tr key={rawKey}>{tds}</tr>);
        }

        return (
            <table className="field-set">{raws}</table>
        );
    }
});

module.exports = FieldSet;