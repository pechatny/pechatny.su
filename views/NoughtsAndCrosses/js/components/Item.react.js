// var Header = require('./Header.react');
// var List = require('./List.react');
var React = require('react');
var Actions = require('../actions/Actions');

var Item = React.createClass({
    render: function(){
        return (
            <td
                key={this.props.id}
                className={this.props.type}
                onClick={this._onClick}
            >
            </td>
        );
    },

    _onClick: function () {
        Actions.setField(this.props.rawIndex, this.props.columnIndex)
    }
});

module.exports = Item;