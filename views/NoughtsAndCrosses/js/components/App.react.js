// var Header = require('./Header.react');
var FieldSet = require('./FieldSet.react');
var React = require('react');

var Store = require('../stores/Store');
var Actions = require('../actions/Actions');

function getState(){
    return {
        data: Store.getAll(),
        resetButton: Store.getResetButton(),
        turnName: Store.getTurnName(),
        winnerName: Store.getWinnerName(),
    }
}

var App = React.createClass({
    getInitialState: function() {
        return getState();
    },

    componentDidMount: function() {
        Store.addChangeListener(this._onChange);
    },

    render: function(){
        var buttonClassName = 'btn ' + this.state.resetButton;
        var winner = '';
        if(this.state.winnerName){
            winner = 'Победили ' + this.state.winnerName
        }

        return (
            <div className="main">
                <div className="left">
                    <FieldSet data={this.state.data}/>
                    <button className={buttonClassName} onClick={this._resetGame}>Начать новую игру</button>
                </div>
                <div className="right">
                    <div className="center">
                        Сейчаc ходят <br/> <span className="red">{this.state.turnName}</span><br/>
                        {winner}
                    </div>
                </div>
            </div>
        );
    },

    _onChange: function () {
        this.setState(getState());
    },

    _resetGame: function(){
        Actions.reset();
    }
});

module.exports = App;