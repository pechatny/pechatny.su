var AppDispatcher = require('../dispatcher/AppDispatcher');

var Actions = {

  /**
   * @param  {string} text
   */
    setField: function(rawIndex, columnIndex) {
      AppDispatcher.dispatch({
        actionType: 'setField',
            rawIndex: rawIndex,
            columnIndex: columnIndex
      });
    },

    reset: function() {
        AppDispatcher.dispatch({
            actionType: 'reset',
        });
    },

    destroy: function(id) {
        console.log('destroy' + id);
        AppDispatcher.dispatch({
            actionType: 'destroy',
            id: id
        });
    }
};

module.exports = Actions;

